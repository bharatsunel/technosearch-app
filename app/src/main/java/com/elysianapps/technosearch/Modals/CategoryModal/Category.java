
package com.elysianapps.technosearch.Modals.CategoryModal;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.Keep;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
@Keep
@Entity  public class Category implements Serializable {

    @PrimaryKey
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("imageurl")
    @Expose
    private String imageurl;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }

    @Override public String toString() {
        return "Category{"
            + "name='"
            + name
            + '\''
            + ", description='"
            + description
            + '\''
            + ", imageurl='"
            + imageurl
            + '\''
            + '}';
    }
}
