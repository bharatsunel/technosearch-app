
package com.elysianapps.technosearch.Modals;

import android.support.annotation.Keep;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
@Keep
public class Team {

    @SerializedName("data")
    @Expose
    private List<Member> data = null;

    public List<Member> getData() {
        return data;
    }

    public void setData(List<Member> data) {
        this.data = data;
    }

}
