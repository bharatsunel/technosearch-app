
package com.elysianapps.technosearch.Modals.EventModal;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

@Entity public class Event implements Serializable {

    @PrimaryKey
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("slug")
    @Expose
    private String slug;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("problem_statement")
    @Expose
    private String problemStatement;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("longitude")
    @Expose
    private Double longitude;
    @SerializedName("lattitude")
    @Expose
    private Double lattitude;
    @SerializedName("event_datatime")
    @Expose
    private String eventDatatime;
    @SerializedName("name1")
    @Expose
    private String name1;
    @SerializedName("email1")
    @Expose
    private String email1;
    @SerializedName("contact1")
    @Expose
    private String contact1;
    @SerializedName("description1")
    @Expose
    private String description1;
    @SerializedName("name2")
    @Expose
    private String name2;
    @SerializedName("email2")
    @Expose
    private String email2;
    @SerializedName("contact2")
    @Expose
    private String contact2;
    @SerializedName("description2")
    @Expose
    private String description2;
    @SerializedName("faculty")
    @Expose
    private String faculty;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getProblemStatement() {
        return problemStatement;
    }

    public void setProblemStatement(String problemStatement) {
        this.problemStatement = problemStatement;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLattitude() {
        return lattitude;
    }

    public void setLattitude(Double lattitude) {
        this.lattitude = lattitude;
    }

    public String getEventDatatime() {
        return eventDatatime;
    }

    public void setEventDatatime(String eventDatatime) {
        this.eventDatatime = eventDatatime;
    }

    public String getName1() {
        return name1;
    }

    public void setName1(String name1) {
        this.name1 = name1;
    }

    public String getEmail1() {
        return email1;
    }

    public void setEmail1(String email1) {
        this.email1 = email1;
    }

    public String getContact1() {
        return contact1;
    }

    public void setContact1(String contact1) {
        this.contact1 = contact1;
    }

    public String getDescription1() {
        return description1;
    }

    public void setDescription1(String description1) {
        this.description1 = description1;
    }

    public String getName2() {
        return name2;
    }

    public void setName2(String name2) {
        this.name2 = name2;
    }

    public String getEmail2() {
        return email2;
    }

    public void setEmail2(String email2) {
        this.email2 = email2;
    }

    public String getContact2() {
        return contact2;
    }

    public void setContact2(String contact2) {
        this.contact2 = contact2;
    }

    public String getDescription2() {
        return description2;
    }

    public void setDescription2(String description2) {
        this.description2 = description2;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    @Override public String toString() {
        return "Event{"
            + "id="
            + id
            + ", name='"
            + name
            + '\''
            + ", slug='"
            + slug
            + '\''
            + ", category='"
            + category
            + '\''
            + ", problemStatement='"
            + problemStatement
            + '\''
            + ", description='"
            + description
            + '\''
            + ", longitude="
            + longitude
            + ", lattitude="
            + lattitude
            + ", eventDatatime='"
            + eventDatatime
            + '\''
            + ", name1='"
            + name1
            + '\''
            + ", email1='"
            + email1
            + '\''
            + ", contact1='"
            + contact1
            + '\''
            + ", description1='"
            + description1
            + '\''
            + ", name2='"
            + name2
            + '\''
            + ", email2='"
            + email2
            + '\''
            + ", contact2='"
            + contact2
            + '\''
            + ", description2='"
            + description2
            + '\''
            + ", faculty='"
            + faculty
            + '\''
            + '}';
    }
}
