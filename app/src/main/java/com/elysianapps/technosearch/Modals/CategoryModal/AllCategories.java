
package com.elysianapps.technosearch.Modals.CategoryModal;

import android.support.annotation.Keep;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
@Keep
public class AllCategories {

    @SerializedName("data")
    @Expose
    private List<Category> data = null;

    public List<Category> getData() {
        return data;
    }

    public void setData(List<Category> data) {
        this.data = data;
    }

    @Override public String toString() {
        return "AllCategories{" + "data=" + data + '}';
    }
}
