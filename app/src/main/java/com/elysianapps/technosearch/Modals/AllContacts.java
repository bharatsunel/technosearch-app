
package com.elysianapps.technosearch.Modals;

import android.support.annotation.Keep;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
@Keep
public class AllContacts {

    @SerializedName("data")
    @Expose
    private List<Contact> data = null;

    public List<Contact> getData() {
        return data;
    }

    public void setData(List<Contact> data) {
        this.data = data;
    }

}
