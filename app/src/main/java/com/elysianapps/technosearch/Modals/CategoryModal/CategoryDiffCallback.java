package com.elysianapps.technosearch.Modals.CategoryModal;

import android.support.annotation.Keep;
import android.support.v7.util.DiffUtil;
import java.util.List;

/**
 * Created by bharatsunel on 10/6/2017.
 */
@Keep
public class CategoryDiffCallback extends DiffUtil.Callback {
  private List<Category> mOldList;
  private List<Category> mNewList;

  public CategoryDiffCallback(List<Category> oldList, List<Category> newList) {
    mOldList = oldList;
    mNewList = newList;
  }

  @Override public int getOldListSize() {
    return mOldList == null ? 0 : mOldList.size();
  }

  @Override public int getNewListSize() {
    return mNewList == null ? 0 : mNewList.size();
  }

  @Override public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {

    //boolean flag = TextUtils.equals(mOldList.get(oldItemPosition).getPlaylistId(),
    //    mNewList.get(newItemPosition).getPlaylistId());
    //
    //LogArsenal.debugOut(
    //    "are Item same " + mOldList.get(oldItemPosition).getPlaylistId() + "---" + mNewList.get(
    //        newItemPosition).getPlaylistId() + "---" + flag);

    return mOldList.get(oldItemPosition)
        .getName()
        .equals(mNewList.get(newItemPosition).getName());
  }

  @Override public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
    return mOldList.get(oldItemPosition).equals(mNewList.get(newItemPosition));
  }

}
