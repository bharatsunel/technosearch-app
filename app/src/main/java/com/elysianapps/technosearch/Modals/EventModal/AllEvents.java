
package com.elysianapps.technosearch.Modals.EventModal;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AllEvents {

    @SerializedName("data")
    @Expose
    private List<Event> data = null;

    public List<Event> getData() {
        return data;
    }

    public void setData(List<Event> data) {
        this.data = data;
    }

}
