package com.elysianapps.technosearch.API;

import com.elysianapps.technosearch.AppConstants;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Bharat on 2/24/2017.
 */

public class RestApiManager {

  private static  TechnoAPI technoAPI;

  public TechnoAPI getTechnoAPI() {
    if (technoAPI == null) {

      OkHttpClient.Builder mHttpClient =
          new OkHttpClient.Builder().readTimeout(120, TimeUnit.SECONDS)
              .writeTimeout(120, TimeUnit.SECONDS);

      mHttpClient.addInterceptor(new LoggingInterceptor());

      Retrofit client = new Retrofit.Builder()
          .baseUrl(AppConstants.BASE_URL)
          .addConverterFactory(GsonConverterFactory.create())
          .client(mHttpClient.build())
          .build();

      technoAPI = client.create(TechnoAPI.class);

    }
    return technoAPI;
  }
}
