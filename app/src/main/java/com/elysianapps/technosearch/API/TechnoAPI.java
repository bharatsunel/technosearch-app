package com.elysianapps.technosearch.API;

import com.elysianapps.technosearch.Modals.AllContacts;
import com.elysianapps.technosearch.Modals.CategoryModal.AllCategories;
import com.elysianapps.technosearch.Modals.EventModal.AllEvents;
import com.elysianapps.technosearch.Modals.Team;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Bharat on 2/24/2017.
 */
//https://www.googleapis.com/youtube/v3/playlistItems?part=snippet%20&key=AIzaSyCsKgVWQNjmQm4xWgTFmQKLRrdoFUif4PY&maxResults=50&alt=json&playlistId=
public interface TechnoAPI {

  @GET("api/categories/{categoryName}/events") Call<AllEvents> getAllEvents(@Path("categoryName") String categoryname );
  @GET("api/categories") Call<AllCategories> getAllCategories();

  @POST("api/contacts")  Call<AllContacts> sendContactUsDeatil(@Query("name") String name,@Query("email") String email,@Query("contact") String contact,@Query("message") String message);

  @GET("api/team") Call<Team> getTeam();

}
