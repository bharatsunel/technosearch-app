package com.elysianapps.technosearch.Adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.elysianapps.technosearch.AppConstants;
import com.elysianapps.technosearch.Modals.Coordinator;
import com.elysianapps.technosearch.R;
import java.util.Collections;
import java.util.List;

/**
 * Created by bharatsunel on 9/29/2017.
 */

public class CoordinatorAdapter extends RecyclerView.Adapter<CoordinatorAdapter.ViewHolder> {
  private List<Coordinator> mCoordinatorList = Collections.emptyList();

  private Context mContext;
  private CallInterface callInterface;

  public CoordinatorAdapter(List<Coordinator> mCoordinatorList, Context mContext,
      CallInterface callInterface) {
    this.mCoordinatorList = mCoordinatorList;
    this.mContext = mContext;
    this.callInterface = callInterface;
  }

  @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.coordinator_row,parent,false);
    return new ViewHolder(view);
  }

  @Override public void onBindViewHolder(ViewHolder holder, int position) {


    Coordinator coordinator = mCoordinatorList.get(position);

    if (coordinator !=null&&!coordinator.getName().equals("NULL")){
      //Glide.with(mContext).load(AppConstants.BASE_URL+"/images/team/"+coordinator.getName()+".jpg").apply(RequestOptions.circleCropTransform()).into(holder.imageView);
      if (coordinator.getName()!=null) holder.name.setText(coordinator.getName().trim());
      if (coordinator.getDescription()!=null) holder.description.setText(coordinator.getDescription().trim());
      if (coordinator.getEmail()!=null) holder.email.setText(coordinator.getEmail().trim());
    }

  }

  @Override public int getItemCount() {
    return mCoordinatorList!=null?mCoordinatorList.size():0;

  }

  public class ViewHolder extends RecyclerView.ViewHolder {
    ImageView imageView,call;
    TextView name, email, description;

    public ViewHolder(View itemView) {
      super(itemView);
     Typeface typeface = Typeface.createFromAsset(mContext.getAssets(), "Roboto-Light.ttf");
      imageView=itemView.findViewById(R.id.im_coordinator_image);
      name=itemView.findViewById(R.id.tv_coordinator_name);
      email =itemView.findViewById(R.id.tv_email);
      description =itemView.findViewById(R.id.tv_coordinator_info);
call=itemView.findViewById(R.id.call);
      name.setTypeface(typeface);
      email.setTypeface(typeface);
      description.setTypeface(typeface);

      call.setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View view) {
          callInterface.onCallClick(mCoordinatorList.get(getAdapterPosition()));
        }
      });
    }
  }

  public interface CallInterface{
    void onCallClick(Coordinator coordinator);
  }

}
