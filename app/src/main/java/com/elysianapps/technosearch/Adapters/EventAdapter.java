package com.elysianapps.technosearch.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.elysianapps.technosearch.AppConstants;
import com.elysianapps.technosearch.Modals.EventModal.Event;
import com.elysianapps.technosearch.R;
import java.util.Collections;
import java.util.List;

/**
 * Created by bharatsunel on 9/29/2017.
 */

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.ViewHolder> {

  private List<Event> mEventList = Collections.emptyList();
  private EventInterface mCategoryInterface;
 private Context mContext;

  public EventAdapter(List<Event> mEventList, EventInterface mCategoryInterface,
      Context mContext) {
    this.mEventList = mEventList;
    this.mCategoryInterface = mCategoryInterface;
    this.mContext = mContext;
  }



  public void refreshAdapter(List<Event> events){
    if (mEventList != null) {
      mEventList.clear();
    }

    if (mEventList!=null)
      mEventList=events;
    notifyDataSetChanged();
  }

  @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.event_row,parent,false);
    return new ViewHolder(view);
  }

  @Override public void onBindViewHolder(final ViewHolder holder, int position) {

    Event e=mEventList.get(position);

    if (e!=null){


      Glide.with(mContext).load(AppConstants.BASE_URL+"/images/events/"+e.getSlug()+".jpg").into(holder.imageView);

      holder.name.setText(e.getName());

      if (!e.getEventDatatime().substring(0,3).equals("000"))holder.timing.setText(e.getEventDatatime());

    }

  }

  @Override public int getItemCount() {
    return mEventList !=null? mEventList.size():0;
  }

  public class ViewHolder extends RecyclerView.ViewHolder {
    ImageView imageView;
    TextView name,timing;
    public ViewHolder(View itemView) {
      super(itemView);
      Typeface typeface = Typeface.createFromAsset(mContext.getAssets(), "Roboto-Light.ttf");
      imageView=itemView.findViewById(R.id.im_image_event);
      name=itemView.findViewById(R.id.tv_event_name);
      timing=itemView.findViewById(R.id.tv_event_timimg);
      name.setTypeface(typeface);
      timing.setTypeface(typeface);
      itemView.setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View view) {
          mCategoryInterface.onEventClick(mEventList.get(getAdapterPosition()),imageView);
        }
      });
    }
  }

  public interface EventInterface{
    void onEventClick(Event event,View view);
  }
}
