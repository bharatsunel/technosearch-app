package com.elysianapps.technosearch.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.elysianapps.technosearch.AppConstants;
import com.elysianapps.technosearch.Modals.EventModal.Event;
import com.elysianapps.technosearch.Modals.Member;
import com.elysianapps.technosearch.R;
import java.util.Collections;
import java.util.List;

/**
 * Created by bharatsunel on 10/5/2017.
 */

public class TeamAdapter extends RecyclerView.Adapter<TeamAdapter.ViewHolder> {

  private List<Member> memberList= Collections.emptyList();
  private Context mContext;
  private MemberInterface memberInterface;

  public TeamAdapter(List<Member> memberList, Context mContext, MemberInterface memberInterface) {
    this.memberList = memberList;
    this.mContext = mContext;
    this.memberInterface = memberInterface;
  }

  @Override public TeamAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.row_member,parent,false);
    return new ViewHolder(v);
  }

  @Override public void onBindViewHolder(final TeamAdapter.ViewHolder holder, int position) {

    Member m=memberList.get(position);
    if (m!=null){


      if (m.getCategory()!=null)holder.category.setText(m.getCategory());
      if (m.getName()!=null){
        holder.name.setText(m.getName());

        Glide.with(mContext).load(AppConstants.BASE_URL+"/images/team/"+m.getName().split(" ")[0]+".jpg").asBitmap().centerCrop().into(new BitmapImageViewTarget(holder.pic) {
          @Override
          protected void setResource(Bitmap resource) {
            RoundedBitmapDrawable circularBitmapDrawable =
                RoundedBitmapDrawableFactory.create(mContext.getResources(), resource);
            circularBitmapDrawable.setCircular(true);
            holder.pic.setImageDrawable(circularBitmapDrawable);
          }
        });

/*        Glide.with(mContext).load(AppConstants.BASE_URL+"/images/team/"+m.getName().split(" ")[0]+".jpg").apply(
            RequestOptions.circleCropTransform()).into(holder.pic);*/
      }
      if (m.getDesignation()!=null) holder.designation.setText(m.getDesignation());


    }
  }

  public void refreshAdapter(List<Member> members){
    if (memberList!= null) {
      memberList.clear();
    }

    if (memberList!=null)
      memberList=members;
    notifyDataSetChanged();
  }
  @Override public int getItemCount() {
    return memberList!=null?memberList.size():0;
  }

  public class ViewHolder extends RecyclerView.ViewHolder {
    TextView name,designation,category;
    ImageView pic,fb,call;
    public ViewHolder(View itemView) {
      super(itemView);
      Typeface typeface = Typeface.createFromAsset(mContext.getAssets(), "Roboto-Black.ttf");
      pic=itemView.findViewById(R.id.pic);
      fb=itemView.findViewById(R.id.fb);
      call=itemView.findViewById(R.id.call);
      name=itemView.findViewById(R.id.name);
      name.setTypeface(typeface);
      designation=itemView.findViewById(R.id.designation);
      category=itemView.findViewById(R.id.category);

      fb.setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View view) {
          memberInterface.onFbClick(memberList.get(getAdapterPosition()).getFacebookLink());
        }
      });
      call.setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View view) {
          memberInterface.onCallClick(memberList.get(getAdapterPosition()).getContact());
        }
      });
    }
  }

  public interface MemberInterface {
    void onFbClick(String url);
    void onCallClick(String phone);
  }
}
