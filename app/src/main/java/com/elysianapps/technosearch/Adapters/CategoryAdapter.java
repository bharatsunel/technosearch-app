package com.elysianapps.technosearch.Adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.elysianapps.technosearch.AppConstants;
import com.elysianapps.technosearch.Modals.CategoryModal.Category;
import com.elysianapps.technosearch.Modals.CategoryModal.CategoryDiffCallback;
import com.elysianapps.technosearch.R;
import java.util.Collections;
import java.util.List;

/**
 * Created by bharatsunel on 9/29/2017.
 */

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {

  private List<Category> mCategoryList = Collections.emptyList();
  private CategoryInterface mCategoryInterface;
  private Context mContext;

  public CategoryAdapter(List<Category> mCategoryList, CategoryInterface mCategoryInterface,
      Context mContext) {
    this.mCategoryList = mCategoryList;
    this.mCategoryInterface = mCategoryInterface;
    this.mContext = mContext;
  }

  @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view =
        LayoutInflater.from(parent.getContext()).inflate(R.layout.category_card_row, parent, false);
    return new ViewHolder(view);
  }



  public void refreshAdapter(@NonNull List<Category> list) {
    DiffUtil.DiffResult diffResult =
        DiffUtil.calculateDiff(new CategoryDiffCallback(mCategoryList, list));
    mCategoryList.clear();
    mCategoryList.addAll(list);
    diffResult.dispatchUpdatesTo(this);
   /* int currentSize = mCategoryList.size();
    mCategoryList.clear();
    mCategoryList.addAll(list);
    //tell the recycler view that all the old items are gone
    notifyItemRangeRemoved(0, currentSize);
    //tell the recycler view how many new items we added
    notifyItemRangeInserted(0, list.size());*/
  }

  @Override public void onBindViewHolder(ViewHolder holder, int position) {

    Category category = mCategoryList.get(position);
    if (category != null) {
      Glide.with(mContext).load(AppConstants.BASE_URL+"/images/categories/" + category.getName() + ".jpg").placeholder(R.drawable.banner).into(holder.imageView);
      if (category.getName()!=null) holder.title.setText(category.getName());
      if (category.getDescription()!=null) holder.description.setText(category.getDescription());
    }
  }

  @Override public int getItemCount() {
    return mCategoryList != null ? mCategoryList.size() : 0;
  }

  public interface CategoryInterface {
    void onCategoryClick(Category category, View imageView, View textView);
  }

  public class ViewHolder extends RecyclerView.ViewHolder {
    TextView title, description;
    ImageView imageView;

    public ViewHolder(View itemView) {
      super(itemView);
      title = itemView.findViewById(R.id.tv_category_title);
      description = itemView.findViewById(R.id.tv_category_description);
      imageView = itemView.findViewById(R.id.im_category);
      Typeface typeface = Typeface.createFromAsset(mContext.getAssets(), "Roboto-Light.ttf");

      title.setTypeface(typeface);
      description.setTypeface(typeface);
      itemView.setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View view) {
          mCategoryInterface.onCategoryClick(mCategoryList.get(getAdapterPosition()), imageView, description);
        }
      });
    }
  }
}
