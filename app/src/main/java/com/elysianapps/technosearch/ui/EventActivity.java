package com.elysianapps.technosearch.ui;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.elysianapps.technosearch.Adapters.CoordinatorAdapter;
import com.elysianapps.technosearch.AppConstants;
import com.elysianapps.technosearch.DB.DatabaseModule;
import com.elysianapps.technosearch.DB.MyDb;
import com.elysianapps.technosearch.Modals.Coordinator;
import com.elysianapps.technosearch.Modals.EventModal.Event;
import com.elysianapps.technosearch.R;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by bharatsunel on 9/29/2017.
 */

public class EventActivity extends AppCompatActivity
    implements  CoordinatorAdapter.CallInterface {
  final private int REQUEST_CODE_ASK_PERMISSIONS = 123;

  // private SupportMapFragment mapFragment;
  private ImageView mImageView;
  private RecyclerView mRecyclerView;
  private CoordinatorAdapter mAdapter;
  private TextView eventName;
  private TextView eventTime;
  private TextView eventDescription;
  private Typeface typefaceBlack;
  private Typeface typefaceLight;
  private List<Coordinator> coordinatorList;
  private CoordinatorAdapter.CallInterface callInterface = this;
  private TextView probStatementLabel;
  private TextView probStatementTv;
  private double latitude = 0.0, longitude = 0.0;
  private TextView probLabel;
private MyDb myDb;
  @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_events);

    initUi();
    setUpUi();
  }

  private void initUi() {
    myDb = DatabaseModule.getInstance(this);
    probLabel = (TextView) findViewById(R.id.tv_prob_label);
    eventName = (TextView) findViewById(R.id.tv_event_name);
    eventTime = (TextView) findViewById(R.id.tv_event_timing);
    eventDescription = (TextView) findViewById(R.id.tv_event_description);
    typefaceBlack = Typeface.createFromAsset(getAssets(), "Roboto-Black.ttf");
    typefaceLight = Typeface.createFromAsset(getAssets(), "Roboto-Light.ttf");
  /*  mapFragment = (SupportMapFragment) getSupportFragmentManager()
        .findFragmentById(R.id.map);
*/
    mImageView = (ImageView) findViewById(R.id.im_event_logo);

    mRecyclerView = (RecyclerView) findViewById(R.id.recyclerview_team);
    coordinatorList = new ArrayList<>();

    probStatementLabel = (TextView) findViewById(R.id.tv_prob_label);
    probStatementTv = (TextView) findViewById(R.id.tv_problem_statement);
  }

  private void setUpUi() {
    eventName.setTypeface(typefaceBlack);
    eventTime.setTypeface(typefaceBlack);
    eventDescription.setTypeface(typefaceLight);
    probStatementLabel.setTypeface(typefaceBlack);
    probStatementTv.setTypeface(typefaceLight);

    //mapFragment.getMapAsync(this);
    findViewById(R.id.fab).setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View view) {
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(
            "http://maps.google.com/maps?daddr=" + latitude + "," + longitude));//20.5666,45.345
        intent.setPackage("com.google.android.apps.maps");
        startActivity(intent);
      }
    });

    Intent i = getIntent();
   Event  event= (Event)i.getSerializableExtra("event");
    Glide.with(this).load(AppConstants.BASE_URL+"/images/events/"+event.getSlug()+".jpg").centerCrop().placeholder(R.drawable.banner).into(mImageView);

    if (event.getName() != null) eventName.setText(event.getName().trim());
    if (event.getEventDatatime() != null && !event.getEventDatatime()
        .substring(0, 3)
        .equals("000")) {
      eventTime.setText(event.getEventDatatime().trim());
    }
    if (event.getDescription() != null) eventDescription.setText(event.getDescription().trim());
    if (event.getProblemStatement() != null && !event.getProblemStatement().equals("NULL")) {
      probStatementTv.setText(event.getProblemStatement().trim());
    } else {
      probStatementTv.setVisibility(View.GONE);
      probLabel.setVisibility(View.GONE);
    }

    if (!event.getName1().equals("NULL"))
    coordinatorList.add(new Coordinator(event.getName1(), event.getEmail1(), event.getContact1(),
        event.getDescription1()));

    if (!event.getName2().equals(""))
      coordinatorList.add(new Coordinator(event.getName2(), event.getEmail2(), event.getContact2(),
          event.getDescription2()));

    latitude = event.getLattitude();
    longitude = event.getLongitude();

    if (latitude==0&&longitude==0) findViewById(R.id.fab).setVisibility(View.GONE);

    mRecyclerView.setHasFixedSize(true);
    mRecyclerView.setLayoutManager(
        new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
    mAdapter = new CoordinatorAdapter(coordinatorList, this, callInterface);
    mRecyclerView.addItemDecoration(
        new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
    mRecyclerView.setAdapter(mAdapter);
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      // Respond to the action bar's Up/Home button
      case android.R.id.home:
        supportFinishAfterTransition();
        return true;
    }
    return super.onOptionsItemSelected(item);
  }

  @Override public void onCallClick(Coordinator coordinator) {

    int hasWriteContactsPermission = 0;
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
      hasWriteContactsPermission = checkSelfPermission(Manifest.permission.CALL_PHONE);

      if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
        requestPermissions(new String[] { Manifest.permission.CALL_PHONE },
            REQUEST_CODE_ASK_PERMISSIONS);
        return;
      }
      callNow(coordinator);
    } else {
      callNow(coordinator);
    }
  }

  private void callNow(Coordinator coordinator) {

    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + coordinator.getContact()));
    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE)
        != PackageManager.PERMISSION_GRANTED) {
      // TODO: Consider calling
      //    ActivityCompat#requestPermissions
      // here to request the missing permissions, and then overriding
      //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
      //                                          int[] grantResults)
      // to handle the case where the user grants the permission. See the documentation
      // for ActivityCompat#requestPermissions for more details.
      Toast.makeText(EventActivity.this, "call permission not given", Toast.LENGTH_LONG).show();
      return;
    }
    startActivity(intent);
  }

  @Override public void onRequestPermissionsResult(int requestCode, String[] permissions,
      int[] grantResults) {
    switch (requestCode) {
      case REQUEST_CODE_ASK_PERMISSIONS:
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
          // Permission Granted
          Toast.makeText(EventActivity.this, "Permission granted Call Now ", Toast.LENGTH_SHORT).show();
        } else {
          // Permission Denied
          Toast.makeText(EventActivity.this, "Call Permission Denied", Toast.LENGTH_SHORT).show();
        }
        break;
      default:
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
  }
}
