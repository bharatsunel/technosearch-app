package com.elysianapps.technosearch.ui;

import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import com.elysianapps.technosearch.API.RestApiManager;
import com.elysianapps.technosearch.Adapters.CategoryAdapter;
import com.elysianapps.technosearch.DB.DatabaseModule;
import com.elysianapps.technosearch.DB.MyDb;
import com.elysianapps.technosearch.Modals.CategoryModal.AllCategories;
import com.elysianapps.technosearch.Modals.CategoryModal.Category;
import com.elysianapps.technosearch.R;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends MyLifeCycleActivity implements CategoryAdapter.CategoryInterface {
  private Toolbar toolbar;
  private FloatingActionButton fab;
  private RecyclerView mRecyclerView;
  private CategoryAdapter mAdapter;
  private CategoryAdapter.CategoryInterface mCategoryInterface = this;
  private MyDb myDb;
  private List<Category> mCategoryList;
  private ProgressBar progressBar;

  @Override protected void onCreate(Bundle savedInstanceState) {
    setTheme(R.style.AppTheme_NoActionBar);
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    initUi();
    setUpUi();
  }

  private void insertData(List<Category> data) {
    if (data != null) {
      new AsyncTask<List<Category>, Void, Void>() {

        @Override protected void onPreExecute() {
        if (mCategoryList.isEmpty()) progressBar.setVisibility(View.VISIBLE);
          Log.d("AsyncTask","onPre execute..");
          super.onPreExecute();
        }

        @Override protected Void doInBackground(List<Category>... lists) {
          Log.d("AsyncTask","doInBackGround execute..");
      Long[] l =     myDb.getLocalDao().insertCategories(lists[0]);

          Log.d("Asynck",l.length+"");
        try{  myDb.getLocalDao().getCategories().getValue().size();}catch (Exception e){}
          return null;
        }

        @Override protected void onPostExecute(Void aVoid) {
          progressBar.setVisibility(View.GONE);
          Log.d("AsyncTask","onPost execute..");
          super.onPostExecute(aVoid);
        }
      }.execute(data);
    }
  }

  private void setUpUi() {

    setSupportActionBar(toolbar);

    fab.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View view) {

        /*Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
            .setAction("Action", null)
            .show();*/
        String url = "http://www.technosearch.in/register";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);

      }
    });

    setUpRecyclerView();

    DatabaseModule.getInstance(this).getLocalDao().getCategories().observe(this, new Observer<List<Category>>() {
      @Override public void onChanged(@Nullable List<Category> categories) {
        if (categories != null) {
          //refresh adapter
          mAdapter.refreshAdapter(categories);
       /*   if (!categories.isEmpty()) mCategoryList.clear();
          if (mCategoryList == null) mCategoryList = new ArrayList<Category>();
          mCategoryList.addAll(categories);
*/

          Log.d("myDb data MainActivity",mCategoryList.toString());
        }else Log.d("Live Data","categories...null...");
        progressBar.setVisibility(View.GONE);
      }
    });


    progressBar.setVisibility(View.VISIBLE);
    new RestApiManager().getTechnoAPI().getAllCategories().enqueue(new Callback<AllCategories>() {
      @Override public void onResponse(Call<AllCategories> call, Response<AllCategories> response) {
        progressBar.setVisibility(View.GONE);
        if (response.isSuccessful()) {

          Log.d("response successfull", response.body().getData().toString());

          if (response.body() != null && response.body().getData()!=null) {

            insertData(response.body().getData());
          }
        } else {

        }
      }

      @Override public void onFailure(Call<AllCategories> call, Throwable t) {

      }
    });
  }

  private void setUpRecyclerView() {
    mRecyclerView.setHasFixedSize(true);
    mRecyclerView.setLayoutManager(
        new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
    mAdapter = new CategoryAdapter(mCategoryList, mCategoryInterface, this);
    SnapHelper snapHelper = new LinearSnapHelper();
    snapHelper.attachToRecyclerView(mRecyclerView);
    mRecyclerView.setAdapter(mAdapter);
  }

  private void initUi() {
    mCategoryList = new ArrayList<>();
    myDb = DatabaseModule.getInstance(this);
    toolbar = (Toolbar) findViewById(R.id.toolbar);
    fab = (FloatingActionButton) findViewById(R.id.fab);

    mRecyclerView = (RecyclerView) findViewById(R.id.recyclerview_categories);
    progressBar = (ProgressBar) findViewById(R.id.progressBar);
  }

  @Override public void onBackPressed() {
    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
    if (drawer.isDrawerOpen(GravityCompat.START)) {
      drawer.closeDrawer(GravityCompat.START);
    } else {
      super.onBackPressed();
    }
  }

  @Override public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.main, menu);
    return true;
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();

    //noinspection SimplifiableIfStatement
    if (id == R.id.action_team) {
      startActivity(new Intent(MainActivity.this,TeamActivity.class));
      return true;
    }else if (id==R.id.action_contact_us){
      startActivity(new Intent(MainActivity.this, ContactUsActivity.class));
    }

    return super.onOptionsItemSelected(item);
  }

  @Override public void onCategoryClick(Category category, View imageView, View textView) {

    Intent intent = new Intent(MainActivity.this, CategoryEventsActivity.class);

    intent.putExtra("category", category);
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
      Pair<View, String> p1 = Pair.create(imageView, "category_image");
      Pair<View, String> p2 = Pair.create(textView, "category_description");
      ActivityOptionsCompat options = ActivityOptionsCompat.
          makeSceneTransitionAnimation(MainActivity.this, p1);
      startActivity(intent, options.toBundle());
    } else {
      startActivity(intent);
    }
  }


}
