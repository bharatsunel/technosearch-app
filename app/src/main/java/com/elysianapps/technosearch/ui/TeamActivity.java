package com.elysianapps.technosearch.ui;

import android.Manifest;
import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.elysianapps.technosearch.API.RestApiManager;
import com.elysianapps.technosearch.Adapters.TeamAdapter;
import com.elysianapps.technosearch.DB.DatabaseModule;
import com.elysianapps.technosearch.DB.MyDb;
import com.elysianapps.technosearch.Modals.Member;
import com.elysianapps.technosearch.Modals.Team;
import com.elysianapps.technosearch.R;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by bharatsunel on 10/5/2017.
 */

public class TeamActivity extends MyLifeCycleActivity implements TeamAdapter.MemberInterface {

  private static final int REQUEST_CODE_ASK_PERMISSIONS = 123;
  private RecyclerView mRecyclerView;

  private TeamAdapter mAdapter;
  private List<Member> memberList;
  private TeamAdapter.MemberInterface memberInterface = this;
  private MyDb myDb;
  private ProgressBar progressBar;
private Toolbar toolbar;

  @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_team);
    initUi();
    setUpUI();
  }

  private void setUpUI() {
    setSupportActionBar(toolbar);
    setTitle("Team");
    toolbar.setTitleTextColor(ContextCompat.getColor(this,android.R.color.white));
    mRecyclerView.setHasFixedSize(true);
    mRecyclerView.setLayoutManager(new GridLayoutManager(this, 3));
    mAdapter = new TeamAdapter(memberList, this, memberInterface);
    mRecyclerView.setAdapter(mAdapter);
mRecyclerView.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));
    myDb.getLocalDao().getAllMembers().observe(this, new Observer<List<Member>>() {
      @Override public void onChanged(@Nullable List<Member> members) {

        if (members != null) {
          //refresh adapter
          mAdapter.refreshAdapter(members);
          if (!members.isEmpty()) memberList.clear();
          if (memberList == null) memberList = new ArrayList<Member>();
          memberList.addAll(members);
        }
      }
    });

    new RestApiManager().getTechnoAPI().getTeam().enqueue(new Callback<Team>() {
      @Override public void onResponse(Call<Team> call, Response<Team> response) {
        if (response.isSuccessful()) {
          if (response.body() != null) insertIntoDB(response.body().getData());
        } else {

        }
      }

      @Override public void onFailure(Call<Team> call, Throwable t) {

      }
    });
  }

  private void insertIntoDB(List<Member> data) {

    new AsyncTask<List<Member>, Void, Void>() {
      @Override protected void onPreExecute() {
        super.onPreExecute();
        if (memberList.isEmpty()) progressBar.setVisibility(View.VISIBLE);
      }

      @Override protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        progressBar.setVisibility(View.GONE);
      }

      @Override protected Void doInBackground(List<Member>... lists) {

        myDb.getLocalDao().insertMembers(lists[0]);

        return null;
      }
    }.execute(data);
  }

  private void initUi() {
    toolbar= (Toolbar) findViewById(R.id.toolbar);
    progressBar = (ProgressBar) findViewById(R.id.progressBar);
    myDb = DatabaseModule.getInstance(this);
    mRecyclerView = (RecyclerView) findViewById(R.id.recycler);
    memberList = new ArrayList<>();
  }

  @Override public void onFbClick(String url) {

    Intent i = new Intent(Intent.ACTION_VIEW);
    i.setData(Uri.parse(url));
    startActivity(i);
  }

  @Override public void onCallClick(String phone) {
    int hasWriteContactsPermission = 0;
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
      hasWriteContactsPermission = checkSelfPermission(Manifest.permission.CALL_PHONE);

      if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
        requestPermissions(new String[] { Manifest.permission.CALL_PHONE },
            REQUEST_CODE_ASK_PERMISSIONS);
        return;
      }
      callNow(phone);
    } else {
      callNow(phone);
    }
  }


  @Override public void onRequestPermissionsResult(int requestCode, String[] permissions,
      int[] grantResults) {
    switch (requestCode) {
      case REQUEST_CODE_ASK_PERMISSIONS:
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
          // Permission Granted
          Toast.makeText(TeamActivity.this, "Permission granted Call Now ", Toast.LENGTH_SHORT).show();
        } else {
          // Permission Denied
          Toast.makeText(TeamActivity.this, "Call Permission Denied", Toast.LENGTH_SHORT).show();
        }
        break;
      default:
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
  }
  private void callNow(String phone)
  {

    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone));
    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE)
        != PackageManager.PERMISSION_GRANTED) {
      // TODO: Consider calling
      //    ActivityCompat#requestPermissions
      // here to request the missing permissions, and then overriding
      //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
      //                                          int[] grantResults)
      // to handle the case where the user grants the permission. See the documentation
      // for ActivityCompat#requestPermissions for more details.
      Toast.makeText(TeamActivity.this, "call permission not given", Toast.LENGTH_LONG).show();
      return;
    }
    startActivity(intent);
  }
}
