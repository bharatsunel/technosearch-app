package com.elysianapps.technosearch.ui;

import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by bharatsunel on 8/12/2017.
 */

public class MyLifeCycleActivity extends AppCompatActivity implements LifecycleRegistryOwner {
  private final LifecycleRegistry mRegistry = new LifecycleRegistry(this);

  @Override public LifecycleRegistry getLifecycle() {
    return mRegistry;
  }
}
