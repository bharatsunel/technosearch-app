package com.elysianapps.technosearch.ui;

import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.elysianapps.technosearch.API.RestApiManager;
import com.elysianapps.technosearch.Adapters.EventAdapter;
import com.elysianapps.technosearch.AppConstants;
import com.elysianapps.technosearch.DB.DatabaseModule;
import com.elysianapps.technosearch.DB.MyDb;
import com.elysianapps.technosearch.Modals.CategoryModal.Category;
import com.elysianapps.technosearch.Modals.EventModal.AllEvents;
import com.elysianapps.technosearch.Modals.EventModal.Event;
import com.elysianapps.technosearch.R;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by bharatsunel on 9/29/2017.
 */

public class CategoryEventsActivity extends MyLifeCycleActivity
    implements EventAdapter.EventInterface {

  private final String TAG = "CategoryEventsActivity";
  private CollapsingToolbarLayout collapsingToolbarLayout;
  private RecyclerView mRecyclerView;
  private EventAdapter mAdapter;
  private List<Event> mEventList;
  private EventAdapter.EventInterface mEventInterface = this;
  private Typeface typefaceLight;
  private TextView categoryDescription;
  private String categoryName;
  private List<Event> eventList;
  private MyDb myDb;
  private TextView tvNoEvents;
  private ProgressBar progressBar;
private ImageView categoryImage;
  @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_category_events);

    initUi();
    setUpUi();
  }

  private void setUpUi() {



    Intent i = getIntent();
    Category category = (Category) i.getSerializableExtra("category");
    if (category != null && category.getName() != null) {

      categoryName = category.getName();
      Glide.with(this).load(AppConstants.BASE_URL+"/images/categories/" + categoryName + ".jpg")
.placeholder(R.drawable.banner)
          .into(categoryImage);

      collapsingToolbarLayout.setTitle(category.getName());
    }
    collapsingToolbarLayout.setExpandedTitleColor(
        getResources().getColor(android.R.color.transparent));
    setUpRecyclerView();
    categoryDescription.setTypeface(typefaceLight);

    if (category!=null&&category.getDescription()!=null) categoryDescription.setText(category.getDescription().replaceAll("\n","").replaceAll("\t",""));

    myDb.getLocalDao().getEventsByCategoryName(categoryName).observe(this, new Observer<List<Event>>() {
      @Override public void onChanged(@Nullable List<Event> events) {
        if (events != null) {
          //refresh adapter
          mAdapter.refreshAdapter(events);
          if (!events.isEmpty()) eventList.clear();
          if (eventList == null) eventList = new ArrayList<Event>();
          eventList.addAll(events);

         if (!eventList.isEmpty()){
           tvNoEvents.setVisibility(View.GONE);
           mRecyclerView.setVisibility(View.VISIBLE);

         }else {
           tvNoEvents.setVisibility(View.VISIBLE);
           mRecyclerView.setVisibility(View.GONE);
         }

        }else{
          tvNoEvents.setVisibility(View.VISIBLE);
          mRecyclerView.setVisibility(View.GONE);
        }
      }
    });

    if (categoryName != null) {
      Log.d(TAG,"Restrieveing events for category: "+categoryName);


      new RestApiManager().getTechnoAPI()
          .getAllEvents(categoryName)
          .enqueue(new Callback<AllEvents>() {
            @Override public void onResponse(Call<AllEvents> call, Response<AllEvents> response) {
              if (response.isSuccessful()) {
                if (response.body()!=null&&response.body().getData()!=null){
                  Log.d(TAG, "response successsfull " + response.body().getData().toString());




                  insertToDb(response.body().getData());
                }
              }
                else {
                Log.d(TAG, "response not successsfull " + response.message());
              }
            }

            @Override public void onFailure(Call<AllEvents> call, Throwable t) {
              Log.d(TAG, "failure");
            }
          });
    }else  Log.d(TAG,"category name is null");
  }

  private void insertToDb(List<Event> data) {

    new AsyncTask<List<Event>, Void, Void>() {
      @Override protected void onPreExecute() {
       if (eventList.isEmpty())progressBar.setVisibility(View.VISIBLE);
        super.onPreExecute();
      }

      @Override protected Void doInBackground(List<Event>... lists) {

        myDb.getLocalDao().insertEvents(lists[0]);

        return null;
      }

      @Override protected void onPostExecute(Void aVoid) {
        progressBar.setVisibility(View.GONE);
        super.onPostExecute(aVoid);
      }
    }.execute(data);
  }

  private void setUpRecyclerView() {
    mRecyclerView.setHasFixedSize(true);
    mRecyclerView.setLayoutManager(
        new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
    mAdapter = new EventAdapter(eventList, mEventInterface, this);
    mRecyclerView.setAdapter(mAdapter);
    if (!eventList.isEmpty()){
      tvNoEvents.setVisibility(View.GONE);
      mRecyclerView.setVisibility(View.VISIBLE);
    }else {
      tvNoEvents.setVisibility(View.VISIBLE);
      mRecyclerView.setVisibility(View.GONE);
    }

  }

  private void initUi() {
    categoryImage= (ImageView) findViewById(R.id.category_image);
    progressBar= (ProgressBar) findViewById(R.id.progressBar);
    tvNoEvents= (TextView) findViewById(R.id.tv_no_events);
    typefaceLight = Typeface.createFromAsset(getAssets(), "Roboto-Light.ttf");
    categoryDescription = (TextView) findViewById(R.id.tv_category_description);
    mRecyclerView = (RecyclerView) findViewById(R.id.recyclerview_events);
    collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
    myDb = DatabaseModule.getInstance(this);
    eventList = new ArrayList<>();
  }

  @Override public void onEventClick(Event event, View view) {
    Intent intent = new Intent(CategoryEventsActivity.this, EventActivity.class);
    intent.putExtra("event", event);

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
      Pair<View, String> p1 = Pair.create(view, "event_image");

      ActivityOptionsCompat options = ActivityOptionsCompat.
          makeSceneTransitionAnimation(CategoryEventsActivity.this, p1);
      startActivity(intent, options.toBundle());
    } else {
      startActivity(intent);
    }
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      // Respond to the action bar's Up/Home button
      case android.R.id.home:
        supportFinishAfterTransition();
        return true;
    }
    return super.onOptionsItemSelected(item);
  }
}
