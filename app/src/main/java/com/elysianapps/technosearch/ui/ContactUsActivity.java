package com.elysianapps.technosearch.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import com.elysianapps.technosearch.API.RestApiManager;
import com.elysianapps.technosearch.Modals.AllContacts;
import com.elysianapps.technosearch.R;
import java.util.regex.Pattern;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by bharatsunel on 9/29/2017.
 */

public class ContactUsActivity extends AppCompatActivity implements View.OnClickListener {
  private Toolbar toolbar;
  private TextInputEditText etName;
  private TextInputEditText etEmail;
  private TextInputEditText etPhone;
  private TextInputEditText etMessage;

  private TextInputLayout tilName;
  private TextInputLayout tilEmail;
  private TextInputLayout tilPhone;
  private TextInputLayout tilMessage;

  private Button btnSend;

  @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_contact_us);
    initUi();
    setUpUi();
  }

  private void setUpUi() {
    setSupportActionBar(toolbar);
    if (getSupportActionBar() != null) getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
    toolbar.setTitleTextColor(ContextCompat.getColor(this, android.R.color.white));
    setTitle("Contact Us!");

    btnSend.setOnClickListener(this);
  }

  private void initUi() {
    toolbar = (Toolbar) findViewById(R.id.toolbar);
    etName = (TextInputEditText) findViewById(R.id.et_name);
    etEmail = (TextInputEditText) findViewById(R.id.et_email);
    etPhone = (TextInputEditText) findViewById(R.id.et_phone);
    etMessage = (TextInputEditText) findViewById(R.id.et_message);

    tilName = (TextInputLayout) findViewById(R.id.til_name);
    tilEmail = (TextInputLayout) findViewById(R.id.til_email);
    tilPhone = (TextInputLayout) findViewById(R.id.til_phone);
    tilMessage = (TextInputLayout) findViewById(R.id.til_message);

    btnSend = (Button) findViewById(R.id.btn_send);
  }

  @Override public void onClick(View view) {



    String name=etName.getText().toString();
    String email=etEmail.getText().toString();
    String phone=etPhone.getText().toString();
    String message=etMessage.getText().toString();

    if (TextUtils.isEmpty(name)) {
      tilName.setError("* required");
    return;
    } else {
      tilName.setError("");

    }
    if (TextUtils.isEmpty(email)) {
      tilEmail.setError("* required");
      return;
    } else {

      tilEmail.setError("");
      if (!isValidEmail(email)){
        tilEmail.setError("* enter a valid email address!");
        return;
      }else tilEmail.setError("");

    }


    if (!TextUtils.isEmpty(phone)&&!isValidMobile(phone)){
      tilPhone.setError("* enter a valid phone number  starts with (91..)!");
      return;
    }else tilPhone.setError("");

    if (TextUtils.isEmpty(message)) {
      tilMessage.setError("* required");
      return;
    } else {
      tilMessage.setError("");
    }

    Toast.makeText(ContactUsActivity.this,"request sending....",Toast.LENGTH_LONG).show();
    new RestApiManager().getTechnoAPI().sendContactUsDeatil(name,email,phone,message).enqueue(new Callback<AllContacts>() {
     @Override public void onResponse(Call<AllContacts> call, Response<AllContacts> response) {
       if (response.isSuccessful()){
         Toast.makeText(ContactUsActivity.this,"request sent",Toast.LENGTH_LONG).show();
       }else Toast.makeText(ContactUsActivity.this,"request sending failed",Toast.LENGTH_LONG).show();
     }

     @Override public void onFailure(Call<AllContacts> call, Throwable t) {
       Toast.makeText(ContactUsActivity.this,"request sending failed",Toast.LENGTH_LONG).show();
     }
   });

  }

  public final static boolean isValidEmail(CharSequence target) {
    if (target == null) {
      return false;
    } else {
      return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }
  }

  private boolean isValidMobile(String phone) {
    boolean check=false;
    if(!Pattern.matches("[a-zA-Z]+", phone)) {
      if(phone.length()< 12||phone.length()>12) {
        // if(phone.length() != 10) {
        check = false;
      } else {
        check = true;
      }
    } else {
      check=false;
    }
    return check;
  }

}
