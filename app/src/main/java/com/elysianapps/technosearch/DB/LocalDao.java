package com.elysianapps.technosearch.DB;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import com.elysianapps.technosearch.Modals.CategoryModal.Category;
import com.elysianapps.technosearch.Modals.EventModal.Event;
import com.elysianapps.technosearch.Modals.Member;
import java.util.List;

/**
 * Created by bharatsunel on 8/12/2017.
 */

@Dao public interface LocalDao {
  @Query("SELECT * FROM Event") LiveData<List<Event>> getEvents();
  @Insert(onConflict = OnConflictStrategy.REPLACE) void insertEvents(List<Event> list);
  @Query("SELECT count(*) FROM Event") int getEventCount();

  @Query("SELECT * FROM Event WHERE category LIKE :categoryname")
  LiveData<List<Event>>  getEventsByCategoryName(String categoryname);


  @Query("SELECT * FROM Category") LiveData<List<Category>> getCategories();
  @Insert(onConflict = OnConflictStrategy.REPLACE) Long[] insertCategories(List<Category> list);

  @Query("SELECT * FROM Member") LiveData<List<Member>> getAllMembers();
  @Insert(onConflict = OnConflictStrategy.REPLACE) void insertMembers(List<Member> list);
}
