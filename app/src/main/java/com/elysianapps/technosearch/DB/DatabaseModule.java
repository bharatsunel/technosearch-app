package com.elysianapps.technosearch.DB;

import android.arch.persistence.room.Room;
import android.content.Context;

/**
 * Created by bharatsunel on 8/12/2017.
 */

public class DatabaseModule {
  private static MyDb  sMyDb;

  public static MyDb getInstance(Context context){
    if(sMyDb==null){
      sMyDb = Room.databaseBuilder(context.getApplicationContext(), MyDb.class,
          "techno_database.db").build();
    }

    return sMyDb;
  }
}
