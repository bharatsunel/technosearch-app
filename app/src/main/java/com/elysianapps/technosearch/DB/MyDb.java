package com.elysianapps.technosearch.DB;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import com.elysianapps.technosearch.Modals.CategoryModal.Category;
import com.elysianapps.technosearch.Modals.EventModal.Event;
import com.elysianapps.technosearch.Modals.Member;

/**
 * Created by bharatsunel on 8/12/2017.
 */
@Database(entities = {Event.class, Category.class, Member.class},version = 6)
public abstract class MyDb extends RoomDatabase {

  public abstract LocalDao getLocalDao();
}
